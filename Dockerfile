FROM debian:buster

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get update && \
    apt-get -y install \
        git build-essential cmake libuv1-dev libssl-dev libhwloc-dev && \
    rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/xmrig/xmrig.git
WORKDIR /xmrig
RUN mkdir build
WORKDIR /xmrig/build
RUN cmake ..
RUN make
COPY run-xmrig.sh .

ENTRYPOINT ["/usr/bin/env"]
CMD ["/bin/bash","-c","./run-xmrig.sh; bash"]
